module.exports =
  APP_URL: 'https://app.upchieve.org'
  # APP_URL: 'http://192.168.0.109.xip.io:8080'
  # APP_URL: 'https://staging.upchieve.org'
  IOS_APP_ID: '1506076042'
  PACKAGE_KEY: 'org.upchieve.mobile'
  CLASS_NAME: 'UPchieve'
  APP_KEY: 'upchieve'
  APP_NAME: 'UPchieve'
  APP_DESCRIPTION: 'Free Math Tutoring'
  SPLASH_COLOR_BG: '#ffffff'
  WINDOW_COLOR_BG: '#ffffff'
  COLOR_PRIMARY: '#16d2aa'
  COLOR_PRIMARY_DARK: '#16d2aa'
  GOOGLE_PROJECT_NUMBER: '877923781231'
  THEME: 'UPchieve'
  ADDITIONAL_PERMISSIONS: null
  ORIENTATION: 'default'
  WK_WEBVIEW_PORT: 12380

  PLATFORM: process.env.PLATFORM or 'native'
